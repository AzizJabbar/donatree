from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def home(request):
    return render(request, 'main/home.html')

def log_in(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("main:home")
        return render(request, "main/login.html", {'success':False})
    return render(request, "main/login.html", {'success':True})

def log_out(request):
    logout(request)
    return redirect("main:home")

def signup(request):
    if request.method == "POST":
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        user = User.objects.create_user(username, email, password)
        user.save()
        return render(request, "main/login.html", {'success':True})
    return render(request, "main/signup.html")

def about(request):
    return render(request, 'main/about.html')